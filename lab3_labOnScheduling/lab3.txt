I. Scheduling simulator: experimenting with FCFS (First-Come, First-Served)

	2. The problem comes from the fact of declaring "name" this way : "char* name;". In fact, this is declaring a pointer named "name" which can point to an array of char anywhere in memory. So we have to assign it to point to an allocated space of memory using malloc before being able to use it. This way, we are able to reallocate "name" later if we need to (it can point to another object). We can declare "name" this way : "char name[MAX_TASK_NAME_SIZE]", but that cannot be reallocated nor resized. We are also using so much memory that we do not usually need.

	3. When running the simulator, the tasks are not run in the FCTS order. In fact, at "currentTime=2", there is only one runnable task which is T2 since "T2.arrivalDate == currentTime", so this task will be run by incrementing "T2.computationsDone" until "T2.computationsDone == T2.totalOfComputations" at "currentTime=13". Then, 2 task (T1 and T3) are runnable but they are not ordered by arrivalDate, so T1 is picked to be run despite "T1.arrivalDate<T3.arrivalDate".
	The solution I suggested for this is to sort the tasks list by "arrivalDate" using the "sortTasks" function that scans all the list (couter i), compares each element to all the other elements after it (counter j) and swaps position if needed. This function is called once at the beginning of the "main" function before callinf the "FCFS" function.

II. Experimenting with other scheduling policies

	1. The SJF policy implementation is similar to the previous one, but this time we have to sort the list by "totalOfComputations". A third argument is added to the function "sortTasks" in order to specify how to sort the tasks. Now at the beginning of the main function, we can choose how to sort the list ( "sortTasks(tasks, nbOfTasks, "arrivalTime")" or "sortTasks(tasks, nbOfTasks, "totalOfComputations")" ).

	2. This time, we have to check at each time slot the "totalOfComputations", that's why I included the sorting function in the "SRTN" function and then check if there is a runnable task with less "totalOfComputations" to run it immediately.